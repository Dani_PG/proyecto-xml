from lxml import etree
doc = etree.parse('gimnasio.xml')
from funciones import *
Fin_programa = False
while not Fin_programa:
    menu()
    opcion = pedir_opcion()
    if opcion == 1:
        opcion1(doc)
    elif opcion == 2:
        lunesaviernes = input("Introduce el día de la semana (Lunes - Martes - Miércoles - Jueves - Viernes): ")
        opcion2(doc,lunesaviernes)
    elif opcion == 3:
        lunesaviernes = input("Introduce el día de la semana (Lunes - Martes - Miércoles - Jueves - Viernes): ")
        opcion3(doc,lunesaviernes)
    elif opcion == 4:
        entrenador = input("Introduce el nombre de un entrenador. Puedes consultarlo en la opción 1: ")
        opcion4(doc,entrenador)
    elif opcion == 5:
        entrenamiento = input("Introduce el nombre de un entrenamiento: ")
        opcion5(doc,entrenamiento)
    elif opcion == 0:
        Fin_programa = True
    else:
        print("Introduce una opción correcta.")
print("Programa finalizado")