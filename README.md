# Proyecto XML

Proyecto XML de un gimnasio

* 1. Listar información: Lista de los nombres y los apellidos de todos los monitores.
* 2. Contar información: Escribe un día de la semana y dará el número de actividades que se realizan ese día.
* 3. Buscar o filtrar información: Escribe un día de la semana y dará los entrenamientos con su hora.
* 4. Buscar información relacionada: Escribe un entrenador y dará los entrenamientos que realiza.
* 5. Ejercicio libre: Escribe un entrenamiento y te dará los días, horas y la sala donde se realizará.
