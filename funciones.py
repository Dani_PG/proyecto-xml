def menu():
    print("")
    print("1. Listar información: Lista de los nombres y los apellidos de todos los monitores.")
    print("2. Contar información: Escribe un día de la semana y dará el número de actividades que se realizan ese día.")
    print("3. Buscar o filtrar información: Escribe un día de la semana y dará los entrenamientos con su hora.")
    print("4. Buscar información relacionada: Escribe un entrenador y dará los entrenamientos que realiza.")
    print("5. Ejercicio libre: Escribe un entrenamiento y te dará los días, horas y la sala donde se realizará.")
    print("0. Salir")
    print("")

def pedir_opcion():
    opcion = int(input("Opción: "))
    while opcion < 0 or opcion > 5:
        opcion = int(input("Opción incorrecta: "))
    return opcion

def nombres(doc):
    nombre = doc.xpath("/gimnasio/monitores/monitor/nombre/text()")
    return nombre

def apellidos(doc):
    apellido = doc.xpath("/gimnasio/monitores/monitor/apellidos/text()")
    return apellido

def comprobar_diasemana(doc,diasemana):
    dias = doc.xpath('//sesion/día/text()')
    if diasemana in dias:
        return True
    else:
        return False

def dias(doc):
    dia = doc.xpath("/gimnasio/actividades/actividad/horario/sesion/día/text()")
    return dia

def actividad_dia(doc,dias_de_la_semana):
    actividad = doc.xpath('//sesion[día="%s"]/../../nombre/text()'%dias_de_la_semana)
    return actividad

def horas_actividad(doc,actividad,dia_valido):
    hora_actividad = doc.xpath('/gimnasio/actividades/actividad[nombre="%s"]/horario/sesion[día="%s"]/horas/hora/text()'%(actividad,dia_valido))
    return hora_actividad

def sala_entrenos(doc,entrenamiento):
    sala = doc.xpath('//actividades/actividad[nombre="%s"]/./lugar/text()'%entrenamiento)
    return sala

def entrenos_entrenador(doc,cod_entrenador):
    entrenamientos = doc.xpath('//actividades/actividad/monitores/monitor[@codigo="%s"]//..//..//nombre/text()'%cod_entrenador)
    return entrenamientos

def dias_entrenos(doc,entrenamiento):
    dias = doc.xpath('//actividades/actividad[nombre="%s"]/./horario/sesion/día/text()'%entrenamiento)
    return dias

def comprobar_entrenador(doc,entrenador):
    entrenadores = doc.xpath('//gimnasio/monitores/monitor/nombre/text()')
    if entrenador in entrenadores:
        return True
    else:
        return False

def guardar_cod_entrenador(doc,entrenador):
    codigo = doc.xpath('//gimnasio/monitores/monitor[nombre="%s"]/./@codigo'%entrenador)
    return codigo[0]

def comprobar_entrenamiento(doc,entrenamiento):
    entrenamientos = doc.xpath('//actividades/actividad/nombre/text()')
    if entrenamiento in entrenamientos:
        return True
    else:
        return False

def dia_valido(doc):
    dia = input("Escribe un día de la semana: ")
    dias = doc.xpath("/gimnasio/actividades/actividad/horario/sesion/día/text()")
    if dia in dias:
        return True
    else:
        return False

def opcion1(doc):
    print("Monitores:")
    lista_nombres = []
    lista_apellidos = []
    for nombremonitor in nombres(doc):
        lista_nombres.append(nombremonitor)
    for apellidomonitor in apellidos(doc):
        lista_apellidos.append(apellidomonitor)
    long = len(lista_nombres)
    for i in range (0,long):
        print(lista_nombres[i],lista_apellidos[i])

def opcion2(doc,dias_de_la_semana):
    comprobacion = comprobar_diasemana(doc,dias_de_la_semana)
    if comprobacion == True:
        num_actividades = 0
        for x in dias(doc):
            if x == dias_de_la_semana:
                num_actividades +=1
        if num_actividades == 1:
            print ("Se realiza", num_actividades,"actividad.")
        else:
            print ("Se realizan", num_actividades,"actividades.")

    else:
        print("Día no disponible en el fichero.")

def opcion3(doc,dias_de_la_semana):
    comprobacion = comprobar_diasemana(doc,dias_de_la_semana)
    if comprobacion == True:
        for actividad in actividad_dia(doc,dias_de_la_semana):
            print("")
            print (actividad)
            horas = horas_actividad(doc,actividad,dias_de_la_semana)
            for hora in horas:
                print (hora)
    else:
        print("Día no disponible en el fichero.")

def opcion4(doc,entrenador):
    comprobacion = comprobar_entrenador(doc,entrenador)
    if comprobacion == True:
        cod_entrenador = guardar_cod_entrenador(doc,entrenador)
        print(entrenador,"realiza los siguientes entrenamientos:")
        for x in entrenos_entrenador(doc,cod_entrenador):
            print(x)
    else:
        print("Día no disponible en el fichero.")

def opcion5(doc,entrenamiento):
    comprobacion = comprobar_entrenamiento(doc,entrenamiento)
    if comprobacion == True:
        for sala in sala_entrenos(doc,entrenamiento):
            print ("El entrenamiento",entrenamiento,"se realiza en la sala ","'",sala,"'",end=" ")
            print ("Los siguientes días y horas:")
            for dia in dias_entrenos(doc,entrenamiento):
                print("")
                print(dia)
                horas = horas_actividad(doc,entrenamiento,dia)
                for hora in horas:
                    print(hora)
    else:
        print("No existe ese entrenamiento en el fichero.")